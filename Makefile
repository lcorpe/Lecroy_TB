CFLAGS=-g -Wno-write-strings -std=c++11

lecroy_DAQ:		lecroy_DAQ.o lecroy_tcp.o
	g++ $(CFLAGS) lecroy_DAQ.o lecroy_tcp.o -o lecroy_DAQ

lecroy_DAQ.o:	lecroy_DAQ.cpp
	g++ $(CFLAGS) -c lecroy_DAQ.cpp lecroy_tcp.cpp

lecroy_tcp.o:	lecroy_tcp.cpp lecroy_tcp.h
	g++ $(CFLAGS) -c lecroy_tcp.cpp
clean:	
	rm *.o
install:
	echo "not going to install anything"
